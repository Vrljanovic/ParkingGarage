module ParkingGarage {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.logging;
    requires annotations;
    opens Controllers;
    opens Vehicles;
    opens UserApp;
}