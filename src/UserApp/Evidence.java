package UserApp;

import Controllers.Main;
import Logger.ErrorLogger;
import Vehicles.Vehicle;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.time.Clock;
import java.util.logging.Level;

public abstract class Evidence {

    static void recordAccident(Vehicle policeVehicle, Vehicle vehicle1, Vehicle vehicle2){
        File accidentFile = new File(System.getProperty("user.home") + File.separatorChar + "accident_" + System.currentTimeMillis() + ".bin");
        if(!accidentFile.exists()) {
            try {
                accidentFile.createNewFile();
            } catch (IOException ex) {
                ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
            }
        }
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(accidentFile));
            objectOutputStream.writeObject(Clock.systemDefaultZone());
            objectOutputStream.writeObject(policeVehicle);
            objectOutputStream.writeObject(vehicle1);
            objectOutputStream.writeObject(vehicle2);
            objectOutputStream.close();
        }
        catch (Exception ex) {
            ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
        }
    }

    static void wantedVehicle(Vehicle policeVehicle, @NotNull Vehicle wantedVehicle) {
        File recordFile = new File(System.getProperty("user.home") + File.separatorChar + "wanted_" + wantedVehicle.getLicencePlate() + "_" + System.currentTimeMillis() + ".bin");
        if (!recordFile.exists()) {
            try {
                recordFile.createNewFile();
            } catch (IOException ex) {
                ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
            }
        }
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(recordFile));
            objectOutputStream.writeObject(Clock.systemDefaultZone());
            objectOutputStream.writeObject(policeVehicle);
            objectOutputStream.writeObject(wantedVehicle);
            objectOutputStream.close();
        } catch (Exception ex) {
            ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
        }
    }
}
