package UserApp;

import Logger.ErrorLogger;
import Vehicles.Car;
import Vehicles.Van;
import Vehicles.Vehicle;
import java.util.Random;
import java.util.logging.Level;

public class InstitutionVehicle extends MovingVehicle implements Runnable {
    private int crashPlatform;
    private int crashRow;
    private int crashColumn;

    public int getCrashPlatform() {
        return crashPlatform;
    }

    public void setCrashPlatform(int crashPlatform) {
        this.crashPlatform = crashPlatform;
    }

    public int getCrashRow() {
        return crashRow;
    }

    public void setCrashRow(int crashRow) {
        this.crashRow = crashRow;
    }

    public int getCrashColumn() {
        return crashColumn;
    }

    public void setCrashColumn(int crashColumn) {
        this.crashColumn = crashColumn;
    }


    private InstitutionVehicle(Vehicle vehicle, int crashPlatform, int crashRow, int crashColumn, String type){
        super(vehicle, 1, 1, 0, VehicleState.EMERGENCY);
        this.crashPlatform = crashPlatform;
        this.crashRow = crashRow;
        this.crashColumn = crashColumn;
    }

    public static void sendHelp(int crashPlatform, int crashRow, int crashColumn){
        try {
            new InstitutionVehicle(new Car("PoliceCar", "", "", "P00A00", 1, true, 0, 0, 5, null), crashPlatform, crashRow, crashColumn, "P").start();
            Thread.sleep(Garage.speedLimit*2);
            new InstitutionVehicle(new Car("AmbulanceCar", "", "", "A00A00", 3, true, 0, 0, 5, null), crashPlatform, crashRow, crashColumn, "A").start();
            Thread.sleep(Garage.speedLimit*2);
            new InstitutionVehicle(new Van("FirefighterVan", "", "", "F00A00", 2, true, 0, 0, 1000, null), crashPlatform, crashRow, crashColumn, "F").start();
        }
        catch (InterruptedException ex){
            ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
        }
    }

    @Override
    public void run() {
        boolean end = false;
        Random random = new Random();
        try {
            while (!end) {
                int oldPlatform = platform, oldRow = row, oldColumn = column;
                int absRow = Math.abs(crashRow - row);
                int absColumn = Math.abs(crashColumn - column);
                if (platform == crashPlatform && (absColumn + absRow < 2)) {
                    end = true;
                    Thread.sleep(random.nextInt(7000) + 3000);
                }
                else if (platform < crashPlatform) {
                    if (column == 7) {
                        column = 0;
                        row = 1;
                        ++platform;
                    } else
                        ++column;
                } else {
                    if (crashRow < 2)
                        ++column;
                    else if (crashColumn < 3) {
                        if (column < 1)
                            ++column;
                        else
                            ++row;
                    } else if (crashColumn < 7)
                        if (column < 5)
                            ++column;
                        else
                            ++row;
                }
                if (!end)
                    Garage.occupyPosition(this, platform, row, column);
                Garage.freePosition(oldPlatform, oldRow, oldColumn);
                sleep(Garage.speedLimit);
            }
            if(movingVehicle.getType() == "PR"){
                Garage.unblock(crashPlatform);
                Evidence.recordAccident(movingVehicle, RandomVehicleGenerator.generateNewVehicle(), RandomVehicleGenerator.generateNewVehicle());
                movingVehicle.setRotationOn(false);
                new MovingVehicle(movingVehicle, platform, row, column, VehicleState.LEAVING).start();
                Thread.sleep(Garage.speedLimit);
            }
            else if(movingVehicle.getType() == "AR"){
                movingVehicle.setRotationOn(false);
                new MovingVehicle(movingVehicle, platform, row, column, VehicleState.LEAVING).start();
                Thread.sleep(Garage.speedLimit);
            }
            else{
                movingVehicle.setRotationOn(false);
                new MovingVehicle(movingVehicle, platform, row, column, VehicleState.LEAVING).start();
                Thread.sleep(Garage.speedLimit);
            }
        }catch(InterruptedException ex){
            ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
        }
    }
}
