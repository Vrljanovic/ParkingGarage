package UserApp;

import Logger.ErrorLogger;
import Vehicles.Vehicle;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;

public abstract class Payment {
    private static ConcurrentHashMap<String, Long> timeWhenVehiclesEnter;
    private static PrintWriter printWriter;

    static{
        try{
            printWriter =new PrintWriter(new FileWriter("payments.csv", true), true);
        }catch(IOException ex){
            ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
            System.exit(1);
        }
        try(ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream("payments.ser"))){
            timeWhenVehiclesEnter = (ConcurrentHashMap<String, Long>)objectInputStream.readObject();
        }catch(Exception ex){
            ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
            timeWhenVehiclesEnter = new ConcurrentHashMap<>();
        }
    }

    public static void add(@NotNull Vehicle vehicle){
        if(vehicle.getTypeAndPriority() == 0)
            timeWhenVehiclesEnter.put(vehicle.getLicencePlate(), System.currentTimeMillis());
    }

    static void charge(@NotNull Vehicle vehicle){
        int charge = 0;
        if(vehicle.getTypeAndPriority() == 0){
            long elapsed = System.currentTimeMillis() - timeWhenVehiclesEnter.get(vehicle.getLicencePlate());
            while(elapsed > 24 * 60 * 60 * 1000){
                charge += 8;
                elapsed += 24 * 60 * 60 * 1000;
            }
            if(elapsed < 60 * 60 * 1000)
                charge += 1;
            else if(elapsed < 3 * 60 * 60 * 1000)
                charge += 2;
            else
                charge += 8;
            printWriter.println(vehicle.getLicencePlate() + "," + charge + ".00KM");
        }
    }

    public static void serializePayment(){
        try{
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("payments.ser"));
            objectOutputStream.writeObject(timeWhenVehiclesEnter);
            objectOutputStream.close();
            printWriter.close();
        }catch (Exception ex){
            ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
        }
    }
}
