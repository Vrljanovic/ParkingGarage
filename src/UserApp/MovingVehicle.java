package UserApp;

import Logger.ErrorLogger;
import Vehicles.Vehicle;

import java.util.logging.Level;

public class MovingVehicle extends Thread {
    protected Vehicle movingVehicle;
    protected int platform;
    protected int row;
    protected int column;
    protected VehicleState state;
    public boolean crashed;

    public MovingVehicle(Vehicle movingVehicle, int platform, int row, int column, VehicleState state) {
        this.movingVehicle = movingVehicle;
        this.platform = platform;
        this.row = row;
        this.column = column;
        this.state = state;
        crashed = false;
    }

    public Vehicle getMovingVehicle() {
        return movingVehicle;
    }

    public void setMovingVehicle(Vehicle movingVehicle) {
        this.movingVehicle = movingVehicle;
    }

    public int getPlatform() {
        return platform;
    }

    public void setPlatform(int platform) {
        this.platform = platform;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    @Override
    public void run() {
        if (state == VehicleState.LEAVING) {
            Payment.charge(movingVehicle);
            boolean left = false;
            while (!left && (state == VehicleState.LEAVING)) {
                if (row == 0 && column == 0 && platform == 1) {
                    left = true;
                    Garage.freePosition(1, 0, 0);
                } else {
                    try {
                        if (movingVehicle.getParkingSpot() != 0)
                            exitParkingSpot();
                        else
                            leaveGarage();
                        sleep(Garage.speedLimit);
                    } catch (Exception ex) {
                        ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
                    }
                }
            }
        } else if (state == VehicleState.ENTERING) {
            while (state == VehicleState.ENTERING && !crashed) {
                try {
                    park();
                    sleep(Garage.speedLimit);
                } catch (InterruptedException ex) {
                    ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
                }
            }
        }
    }

    private void leaveGarage() throws InterruptedException {
        int oldRow = row, oldPlatform = platform, oldColumn = column;
        if (row > 0 && (column == 1 || column == 5))
            ++column;
        else if (row > 0 && (column == 2 || column == 6))
            --row;
        else if (row == 9 && column > 2)
            ++column;
        else if (row == 0 && column > 0)
            --column;
        else if (row == 0 && column == 0 && platform > 1) {
            --platform;
            row = 0;
            column = 7;
        }
        Garage.occupyPosition(this, platform, row, column);
        Garage.freePosition(oldPlatform, oldRow, oldColumn);
    }

    private void park() throws InterruptedException {
        int oldRow = row, oldPlatform = platform, oldColumn = column;
        int destinationRow = Garage.getRow(movingVehicle.getParkingSpot());
        int destinationColumn = Garage.getColumn(movingVehicle.getParkingSpot());
        if (platform == movingVehicle.getPlatformNumber() && row == destinationRow && column == destinationColumn) {
            state = VehicleState.PARKED;
            return;
        } else if (platform == movingVehicle.getPlatformNumber()) {
            if (destinationColumn == 0 || destinationColumn == 3) {
                if (column < 1)
                    ++column;
                else if (column == 1 && row < destinationRow)
                    ++row;
                else {
                    if (destinationColumn == 0)
                        --column;
                    else
                        ++column;
                }
            } else {
                if (column < 5)
                    ++column;
                else if (column == 5 && row < destinationRow)
                    ++row;
                else {
                    if (destinationColumn == 4)
                        --column;
                    else
                        ++column;
                }
            }
        } else {
            if (column == 7 && row == 1 && platform < movingVehicle.getPlatformNumber()) {
                row = 1;
                column = 0;
                ++platform;
            } else
                ++column;
        }
        Garage.occupyPosition(this, platform, row, column);
        Garage.freePosition(oldPlatform, oldRow, oldColumn);
    }

    private void exitParkingSpot() throws InterruptedException {
        int oldRow = row, oldColumn = column, oldPlatform = platform;
        if (column == 0 || column == 1 || column == 4 || column == 5)
            ++column;
        else if (column == 3 || column == 7)
            --column;
        else {
            movingVehicle.setParkingSpot(0);
            return;
        }
        Garage.occupyPosition(this, platform, row, column);
        Garage.freePosition(oldPlatform, oldRow, oldColumn);
    }
}