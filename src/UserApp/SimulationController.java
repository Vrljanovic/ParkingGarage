package UserApp;

import Logger.ErrorLogger;
import Vehicles.Vehicle;
import Controllers.AdministratorController;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Paint;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.net.URL;
import java.util.*;
import java.util.logging.Level;

public class SimulationController implements javafx.fxml.Initializable {

    @FXML
    private AnchorPane anchorPane;

    @FXML
    private ComboBox<Integer> numberOfPlatformsComboBox;

    @FXML
    private GridPane gridPane;

    @FXML
    private Button addNewVehicleButton;

    @FXML
    private Button closeAppButton;

    @FXML
    private Button startButton;

    @FXML
    private TextField numberTextField;

    private static ArrayList<Label> blueLabels = new ArrayList<>();
    private static ArrayList<Label> grayLabels = new ArrayList<>();
    private static final int numberOfPlatforms = AdministratorController.numberOfPlatforms;
    private static final ListView[] parkedVehicles = AdministratorController.parkedVehiclesList;
    private static int platformNumber = 1;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Garage.initializeGarage();
        initializeComboBox();
        gridPane = new GridPane();
        setGridPane(gridPane);
        gridPane.setVisible(true);
        gridPane.setLayoutX(180);
        gridPane.setLayoutY(100);
        gridPane.setPrefSize(300, 250);
        gridPane.setGridLinesVisible(true);
        anchorPane.getChildren().add(gridPane);
        showPlatform(1);
    }

    private void initializeComboBox() {
        for (int i = 1; i <= numberOfPlatforms; ++i) {
            numberOfPlatformsComboBox.getItems().add(i);
        }
        numberOfPlatformsComboBox.setValue(platformNumber);
    }

    public void showSelectedPlatform(){
        platformNumber = numberOfPlatformsComboBox.getValue();
        showPlatform(platformNumber);
    }

    public void saveAndQuit() {
        Payment.serializePayment();
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("garage.ser");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            LinkedList<Vehicle> list = new LinkedList<>();
            for (int i = 0; i < numberOfPlatforms; ++i) {
                ObservableList<Vehicle> vehicles = parkedVehicles[i].getItems();
                for (Vehicle veh : vehicles)
                    list.add(veh);
            }
            objectOutputStream.writeObject(list);
            objectOutputStream.close();
            fileOutputStream.close();
        } catch (Exception ex) {
            ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
            System.exit(-1);
        }
        System.exit(0);
    }

    public void startSimulation() throws Exception{
        setMinimumCars();
        Garage.initializeGarage();
        addNewVehicleButton.setDisable(false);
        new Thread(() -> {
            while (true) {
                Platform.runLater(() -> showPlatform(platformNumber));
                try {
                    Thread.sleep(Garage.speedLimit);
                } catch (Exception ex) {
                    ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
                }
            }
        }).start();
        startButton.setDisable(true);
        pickVehiclesToExit();
    }

    private void setGridPane(GridPane gridPane) {
        int parkingSpot = 1;
        Border border = new Border(new BorderStroke(Paint.valueOf("Gray"), BorderStrokeStyle.DASHED, null, BorderStroke.MEDIUM));
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 10; j++) {
                final Label label = new Label();
                label.setPrefSize(30, 30);
                label.setAlignment(Pos.CENTER);
                label.setBorder(border);
                if (((i == 0 || i == 7) && j > 1) || (i == 3 || i == 4) && j > 1 && j < 8) {
                    label.setText(Integer.toString(parkingSpot));
                    ++parkingSpot;
                    label.setStyle("-fx-background-color: darkblue");
                    label.setTextFill(Paint.valueOf("white"));
                    blueLabels.add(label);
                } else {
                    label.setStyle("-fx-background-color: gray");
                    label.setTextFill(Paint.valueOf("aqua"));
                    grayLabels.add(label);
                }
                gridPane.add(label, i, j);
            }
        }
    }

    private synchronized static int getGrayLabelIndex(int row, int column){
        if(column == 0)
            return row;
        else if(column == 1)
            return row + 2;
        else if(column == 2)
            return row + 12;
        else if(column == 3){
            if(row < 2)
                return row + 22;
            else
                return row + 16;
        }
        else if(column == 4){
            if(row < 2)
                return row + 26;
            else
                return row + 20;
        }
        else if(column == 5)
            return row + 30;
        else if(column == 6)
            return row + 40;
        else
            return row + 50;
    }

    private synchronized int getParkingSpotNumber(int row, int column){
        if(column == 0)
            return row - 1;
        else if(column == 3)
            return row + 7;
        else if(column == 4)
            return row + 13;
        else
            return row + 19;
    }

    private synchronized void showPlatform(int platformNumber){
        for (int i = 0; i < 10; ++i) {
            for (int j = 0; j < 8; ++j) {
                if (Garage.garage[platformNumber - 1].driveway[i][j] != null) {
                    Vehicle veh = Garage.garage[platformNumber - 1].driveway[i][j].getMovingVehicle();
                    if (Garage.isParkingSpot(i, j)) {
                        blueLabels.get(veh.getParkingSpot() - 1).setText(veh.getType());
                        blueLabels.get(veh.getParkingSpot() - 1).setTextFill(Paint.valueOf("orange"));
                    } else
                        grayLabels.get(getGrayLabelIndex(i, j)).setText(veh.getType());
                } else {
                    if (Garage.isParkingSpot(i, j)) {
                        blueLabels.get(getParkingSpotNumber(i, j) - 1).setText(Integer.toString(getParkingSpotNumber(i, j)));
                        blueLabels.get(getParkingSpotNumber(i, j) - 1).setTextFill(Paint.valueOf("white"));
                    } else
                        grayLabels.get(getGrayLabelIndex(i, j)).setText("");
                }
            }
        }
    }

    public void addNewCar() throws InterruptedException{
        if(Garage.isGarageFull()){
            addNewVehicleButton.setDisable(true);
            Alert alert = new Alert(Alert.AlertType.INFORMATION, null, ButtonType.OK);
            alert.setTitle("Garage");
            alert.setContentText("Garage is full");
            alert.showAndWait();
            return;
        }
        Vehicle newVehicle =  RandomVehicleGenerator.generateNewVehicle();
        Payment.add(newVehicle);
        int[] firstFreeSpot = Garage.searchFirstFreeParkingSpot();
        newVehicle.setPlatformNumber(firstFreeSpot[0]);
        newVehicle.setParkingSpot(firstFreeSpot[1]);
        parkedVehicles[newVehicle.getPlatformNumber() - 1].getItems().add(newVehicle);
        MovingVehicle movingVehicle = new MovingVehicle(newVehicle, 1, 1,0, VehicleState.ENTERING);
        Garage.occupyPosition(movingVehicle, 1, 1,0);
        movingVehicle.start();
    }

    private void setMinimumCars(){
        try{
            int numberOfCars = Integer.parseInt(numberTextField.getText());
            if(numberOfCars > 28 || numberOfCars < 1){
                Alert alert = new Alert(Alert.AlertType.ERROR, null, ButtonType.OK);
                alert.setTitle("Error");
                alert.setContentText("Number of cars can be between 1 and 28");
                alert.showAndWait();
                return;
            }
            for(int i = 0; i < numberOfPlatforms; ++i){
                if(parkedVehicles[i].getItems().toArray().length < numberOfCars){
                    for(int j = parkedVehicles[i].getItems().toArray().length; j < numberOfCars; ){
                        Vehicle vehicle = RandomVehicleGenerator.generateNewVehicle();
                        vehicle.setPlatformNumber(i + 1);
                        if(Garage.isParkingSpotFree(vehicle.getPlatformNumber(), vehicle.getParkingSpot())){
                            ++j;
                            parkedVehicles[i].getItems().add(vehicle);
                            Payment.add(vehicle);
                        }
                    }
                }
            }
        }
        catch(Exception ex){
            ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
            Alert alert = new Alert(Alert.AlertType.ERROR, null, ButtonType.OK);
            alert.setTitle("Error");
            alert.setContentText("Wrong input " + ex.getMessage());
            alert.showAndWait();
        }
    }

    private void pickVehiclesToExit(){
        Random random = new Random();
        LinkedList<MovingVehicle> vehicles = new LinkedList<>();
        for(int i = 0; i < numberOfPlatforms; ++i){
            for(Vehicle veh : (ObservableList<Vehicle>)parkedVehicles[i].getItems()){
                if(random.nextDouble() < 0.15){
                    int row = Garage.getRow(veh.getParkingSpot());
                    int column = Garage.getColumn(veh.getParkingSpot());
                    vehicles.add(new MovingVehicle(veh, veh.getPlatformNumber(), row, column, VehicleState.LEAVING));
                    final int platformIndex = i;
                    Platform.runLater(() -> parkedVehicles[platformIndex].getItems().remove(veh));
                }
            }
        }
        vehicles.forEach(Thread::start);
    }
}