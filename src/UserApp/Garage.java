package UserApp;

import Controllers.Main;
import Logger.ErrorLogger;
import Vehicles.Vehicle;
import Controllers.AdministratorController;
import javafx.application.Platform;
import javafx.collections.ObservableList;
import javafx.scene.control.ListView;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.*;
import java.util.LinkedList;
import java.util.Random;
import java.util.logging.Level;

public abstract class Garage {
    private static final int numberOfPlatforms = AdministratorController.numberOfPlatforms;
    private static ListView<Vehicle>[] parkedVehicles = AdministratorController.parkedVehiclesList;
    static Driveway garage[] = new Driveway[numberOfPlatforms];
    public static final int speedLimit = 400;
    private static boolean[] isBlocked = new boolean[numberOfPlatforms];
    private static boolean[] possibleCrash = new boolean[numberOfPlatforms];
    private static final String wantedVehiclesFileName = "wantedVehicles.txt";
    private static final Random random = new Random();

    static{
        for(int i = 0; i < numberOfPlatforms; ++i)
            possibleCrash[i] = true;
    }

    public static boolean isGarageFull(){
        for(int i = 0; i < numberOfPlatforms; ++i){
            if(parkedVehicles[i].getItems().toArray().length != 28)
                return false;
        }
        return true;
    }

    public static  boolean isParkingSpotFree(int platform, int parkingSpot){
        ObservableList<Vehicle> vehicles = parkedVehicles[platform - 1].getItems();
        for(Vehicle veh : vehicles){
            if(parkingSpot == veh.getParkingSpot())
                return false;
        }
        return true;
    }

    static void initializeGarage() {
        for(int i = 0; i < numberOfPlatforms; ++i){
            garage[i] = new Driveway();
            for(Vehicle veh : parkedVehicles[i].getItems()){
                int parkingSpot = veh.getParkingSpot();
                if(parkingSpot < 9)
                    garage[i].driveway[parkingSpot + 1][0] = new MovingVehicle(veh, i, parkingSpot + 1, 0, VehicleState.PARKED);
                else if(parkingSpot < 15)
                    garage[i].driveway[parkingSpot - 7 ][3] = new MovingVehicle(veh, i, parkingSpot - 7, 3, VehicleState.PARKED);
                else if(parkingSpot < 21)
                    garage[i].driveway[parkingSpot - 13][4] = new MovingVehicle(veh, i, parkingSpot - 13, 4, VehicleState.PARKED);
                else
                    garage[i].driveway[parkingSpot - 19][7] = new MovingVehicle(veh, i, parkingSpot - 19, 7, VehicleState.PARKED) ;
            }
        }
    }

    @Contract(pure = true)
    static boolean isParkingSpot(int row, int column){
        if(row > 1 && (column == 0 || column == 7))
                return true;
        else return row > 1 && row < 8 && (column == 3 || column == 4);
    }

    @Contract(pure = true)
    static int getRow(int parkingSpot){
        if(parkingSpot < 9)
            return parkingSpot + 1;
        if(parkingSpot < 15)
            return parkingSpot - 7;
        if(parkingSpot < 21)
            return parkingSpot - 13;
        return parkingSpot - 19;
    }

    @Contract(pure = true)
    static int getColumn(int parkingSpot){
        if(parkingSpot < 9)
            return 0;
        if(parkingSpot < 15)
            return 3;
        if(parkingSpot < 21)
            return 4;
        return 7;
    }

    static void occupyPosition(@NotNull MovingVehicle movingVehicle, int platform, int row, int column)throws InterruptedException{
        MovingVehicle vehicle = garage[platform - 1].driveway[row][column];
        if(movingVehicle.getMovingVehicle().getType().contains("R")) {
            if(vehicle == null)
                garage[platform - 1].driveway[row][column] = movingVehicle;
            return;
        }
        while(isBlocked[platform - 1]){
            Integer monitor = platform;
            synchronized (monitor){
                monitor.wait();
            }
        }
        boolean crash = random.nextDouble() < 0.1;
        if (crash && possibleCrash[platform - 1] && row > 1 && vehicle != null) {
            Integer monitor = platform;
            synchronized (monitor) {
                possibleCrash[platform - 1] = false;
                isBlocked[platform - 1] = true;
                final Vehicle tempVehicle = vehicle.getMovingVehicle();
                Platform.runLater(() -> {
                    if (parkedVehicles[platform - 1].getItems().contains(movingVehicle.getMovingVehicle()))
                        parkedVehicles[platform - 1].getItems().remove(movingVehicle.getMovingVehicle());
                    if (parkedVehicles[platform - 1].getItems().contains(tempVehicle))
                        parkedVehicles[platform - 1].getItems().remove(tempVehicle);
                });
                movingVehicle.state = VehicleState.LEAVING;
                if (garage[platform - 1].driveway[row][column] != null)
                    garage[platform - 1].driveway[row][column].state = VehicleState.LEAVING;
                InstitutionVehicle.sendHelp(platform, row, column);
                garage[platform - 1].driveway[row][column] = null;
                monitor.wait();
            }
        }
        else  if(vehicle != null) {
            if(movingVehicle.getMovingVehicle().getTypeAndPriority() == 1){
                if(isWantedVehicle(vehicle.getMovingVehicle())){
                    movingVehicle.getMovingVehicle().setRotationOn(true);
                    movingVehicle.state = VehicleState.LEAVING;
                    vehicle.state = VehicleState.LEAVING;
                    garage[platform - 1].driveway[row][column] = vehicle;
                    Evidence.wantedVehicle(movingVehicle.getMovingVehicle(), vehicle.getMovingVehicle());
                    Thread.sleep(random.nextInt(2000) + 3000);
                }
            }
            while (vehicle != null) {
                synchronized (vehicle) {
                    vehicle.wait();
                }
                vehicle = garage[platform - 1].driveway[row][column];
            }
        }
        garage[platform - 1].driveway[row][column] = movingVehicle;
    }

    static void freePosition(int platform, int row, int column){
        MovingVehicle vehicle = garage[platform - 1].driveway[row][column];
        if(vehicle != null) {
            synchronized (vehicle){
                garage[platform - 1].driveway[row][column] = null;
                vehicle.notifyAll();
            }
        }
    }

    @Nullable
    static int[] searchFirstFreeParkingSpot(){
        int[] parkingPlace = new int[2];
        for(int i = 0; i < AdministratorController.numberOfPlatforms; ++i){
            for(int j = 1; j < 29; ++j){
                if(Garage.isParkingSpotFree(i + 1, j)){
                    parkingPlace[0] = i + 1;
                    parkingPlace[1] = j;
                    return parkingPlace;
                }
            }
        }
        return null;
    }

    @NotNull
    @Contract(" -> new")
    public static int[] parkVehicleRandom(){
        while(true) {
            int platform = 1 + random.nextInt(AdministratorController.numberOfPlatforms);
            int parkingSpot = 1 + random.nextInt(28);
            if(Garage.isParkingSpotFree(platform, parkingSpot))
                return new int[]{platform, parkingSpot};
        }
    }

    static void unblock(int platform){
        isBlocked[platform - 1] = false;
        Integer monitor = platform;
        synchronized (monitor){
            monitor.notifyAll();
        }
    }

    private static synchronized boolean isWantedVehicle(Vehicle vehicle){
        File wantedVehiclesFile = new File(wantedVehiclesFileName);
        if(!wantedVehiclesFile.exists()){
           return false;
        }
        else{
            try {
                BufferedReader bufferedReader = new BufferedReader(new FileReader(wantedVehiclesFile));
                LinkedList<String> allWantedVehicles = new LinkedList<>();
                String plate = "";
                while ((plate = bufferedReader.readLine()) != null) {
                    allWantedVehicles.add(plate);
                }
                bufferedReader.close();
                for(String licencePlate : allWantedVehicles){
                    if(licencePlate.equals(vehicle.getLicencePlate()))
                        return true;
                }
                return false;
            }
            catch (Exception ex){
                ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
                return false;
            }
        }
    }
}
