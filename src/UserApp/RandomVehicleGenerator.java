package UserApp;

import Vehicles.Car;
import Vehicles.Motorcycle;
import Vehicles.Van;
import Vehicles.Vehicle;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.Random;

public abstract class RandomVehicleGenerator {
    private static final String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private static final String[] vehicleTypes = {"Motorcycle", "Car", "Van"};
    private static final Random random = new Random();

    @Nullable
    static Vehicle generateNewVehicle(){
        char[] name = new char[6];
        char[] licencePlate = new char[7];
        char[] chassisNumber = new char[17];
        char[] engineNumber = new char[15];
        String vehicleType = vehicleTypes[random.nextInt(3)];

        //Generate Vehicle Name
        name[0] = alphabet.charAt(random.nextInt(26));
        for(int i = 1; i < 6; ++i)
            name[i] = alphabet.charAt(random.nextInt(36));

        //Generate Vehicle Licence Plate
        licencePlate[0] = alphabet.charAt(random.nextInt(36));
        for(int i = 1; i < 3; ++i)
            licencePlate[i] = Integer.toString(random.nextInt(10)).charAt(0);
        licencePlate[3] = alphabet.charAt(random.nextInt(26));
        for(int i = 4; i < 7; ++i)
            licencePlate[i] = Integer.toString(random.nextInt(10)).charAt(0);

        //Generate Vehicle Chassis Number
        for(int i = 0; i < 17; ++i)
            chassisNumber[i] = alphabet.charAt(random.nextInt(36));

        //Generate Engine Number
        for(int i  = 0; i < 15; ++i)
            engineNumber[i] = alphabet.charAt(random.nextInt(36));

        //Take Random Picture
            File[] pictures = new File("Pictures").listFiles((dir, fileName) ->
                    fileName.endsWith(".jpg") || fileName.endsWith(".png") || fileName.endsWith(".bmp"));
        File imageFile = pictures[random.nextInt(pictures.length)];

        int[] parkingPlace = Garage.parkVehicleRandom();
        switch (vehicleType) {
            case "Motorcycle":
                boolean isPolice = random.nextDouble() < 0.1;
                int typeAndPriority = 0;
                if(isPolice)
                    typeAndPriority = 1;
                return new Motorcycle(new String(name), new String(chassisNumber), new String(engineNumber), new String(licencePlate), typeAndPriority, false, parkingPlace[0], parkingPlace[1], imageFile);
            case "Car":
                int numberOfDoors = 2 + random.nextInt(4);
                boolean canBeInstitution = random.nextDouble() < 0.1;
                if(canBeInstitution) {
                    typeAndPriority = random.nextInt(2) + 1;
                    if(typeAndPriority == 2)
                        ++typeAndPriority;
                }
                else
                    typeAndPriority = 0;
                return new Car(new String(name), new String(chassisNumber), new String(engineNumber), new String(licencePlate), typeAndPriority, false, parkingPlace[0], parkingPlace[1], numberOfDoors, imageFile);
            case "Van":
                int capacity = 500 * (1 +random.nextInt(7));
                boolean isInstitutional = random.nextDouble() < 0.1;
                if(isInstitutional) {
                    typeAndPriority = random.nextInt(3);
                    ++typeAndPriority;
                }
                else
                    typeAndPriority = 0;
                return new Van(new String(name), new String(chassisNumber), new String(engineNumber), new String(licencePlate), typeAndPriority, false, parkingPlace[0], parkingPlace[1], capacity, imageFile);
        }
        return null;
    }
}
