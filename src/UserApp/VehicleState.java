package UserApp;

public enum VehicleState {
    ENTERING, LEAVING, PARKED, EMERGENCY
}