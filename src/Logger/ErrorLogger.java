package Logger;

import java.util.logging.*;

public abstract class ErrorLogger {
    public static Logger logger;

    static{
        logger = Logger.getLogger("error.log");
        try {
            Handler fileHandler = new FileHandler("error.log", true);
            logger.addHandler(fileHandler);
            SimpleFormatter formatter = new SimpleFormatter();
            fileHandler.setFormatter(formatter);
        }catch(Exception ex){
            ex.printStackTrace();
            System.exit(-1);
        }
    }
}