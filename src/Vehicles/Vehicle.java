package Vehicles;

import java.io.File;
import java.io.Serializable;

public class Vehicle implements Serializable {
    private String name, chassisNumber, engineNumber, licencePlate;
    private int typeAndPriority; // 0-civil vehicle, 1- police vehicle, 2 - firefighter vehicle, 3 - ambulance
    private boolean isRotationOn;
    private File imageFile;
    private int parkingSpot;
    private int platformNumber;

    protected Vehicle(String name, String chassisNumber, String engineNumber, String licencePlate, int typeAndPriority, boolean isRotationOn, int platformNumber, int parkingSpot, File imageFile) {
        this.name = name;
        this.chassisNumber =chassisNumber;
        this.engineNumber = engineNumber;
        this.licencePlate = licencePlate;
        this.isRotationOn = isRotationOn;
        this.typeAndPriority = typeAndPriority;
        this.platformNumber = platformNumber;
        this.parkingSpot = parkingSpot;
        this.imageFile = imageFile;
    }

    @Override
    public String toString(){
        return name + " - " + licencePlate;
    }

    public int getTypeAndPriority() {
        return typeAndPriority;
    }

    public boolean isRotationOn() {
        return isRotationOn;
    }

    public File getImageFile() {
        return imageFile;
    }

    public void setImage(File image) {
        this.imageFile = image;
    }

    public void setRotationOn(boolean rotationOn) {
        isRotationOn = rotationOn;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }

    public String getEngineNumber() {
        return engineNumber;
    }

    public void setEngineNumber(String engineNumber) {
        this.engineNumber = engineNumber;
    }

    public String getLicencePlate() {
        return licencePlate;
    }

    public void setLicencePlate(String licencePlate) {
        this.licencePlate = licencePlate;
    }

    public int getParkingSpot(){return parkingSpot;}

    public void setParkingSpot(int parkingSpot){this.parkingSpot = parkingSpot;}

    public int getPlatformNumber() {
        return platformNumber;
    }

    public void setPlatformNumber(int platformNumber) {
        this.platformNumber = platformNumber;
    }

    public String getType(){
        if(getTypeAndPriority() == 0)
            return "V";
        if(getTypeAndPriority() == 1){
            if(isRotationOn() == true)
                return "PR";
            else
                return "P";
        }
        if(getTypeAndPriority() == 2){
            if(isRotationOn() == true)
                return "FR";
            else
                return"F";
        }
        else{
            if(isRotationOn() == true)
                return "AR";
            else
                return "A";
        }
    }
}
