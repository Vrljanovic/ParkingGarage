package Vehicles;

import java.io.File;

public class Car extends Vehicle {
    int numberOfDoors; //2,3,4,5

    public Car(String name, String chassisNumber, String engineNumber, String licencePlate, int typeAndPriority, boolean isRotationOn,
               int platformNumber, int parkingSpot, int numberOfDoors, File imageFile){
        super(name, chassisNumber, engineNumber, licencePlate, typeAndPriority, isRotationOn,
                platformNumber, parkingSpot, imageFile);
        this.numberOfDoors = numberOfDoors;
    }

    public int getNumberOfDoors() {
        return numberOfDoors;
    }

    public void setNumberOfDoors(int numberOfDoors) {
        this.numberOfDoors = numberOfDoors;
    }
}
