package Vehicles;

import java.io.File;

public class Motorcycle extends Vehicle {
    public Motorcycle(String name, String chassisNumber, String engineNumber, String licencePlate, int typeAndPriority,
                      boolean isRotationOn,int platformNumber, int parkingSpot, File imageFile) {
        super(name, chassisNumber, engineNumber, licencePlate, typeAndPriority,
                isRotationOn, platformNumber, parkingSpot, imageFile);
    }
}
