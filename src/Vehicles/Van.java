package Vehicles;

import java.io.File;

public class Van extends Vehicle {
    private int capacity;

    public Van(String name, String chassisNumber, String engineNumber, String licencePlate, int typeAndPriority,
               boolean isRotationOn, int platformNumber, int parkingSpot, int capacity, File imageFile){
        super(name, chassisNumber, engineNumber, licencePlate, typeAndPriority,
                isRotationOn, platformNumber, parkingSpot, imageFile);
        this.capacity = capacity;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }
}
