package Controllers;

import Logger.ErrorLogger;
import Vehicles.Vehicle;
import javafx.collections.ObservableList;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

import java.io.*;
import java.net.URL;
import java.util.LinkedList;
import java.util.Properties;
import java.util.ResourceBundle;
import java.util.logging.Level;

public class AdministratorController implements Initializable, Serializable {

    @FXML
    private Pane pane;

    @FXML
    private ComboBox<String> vehicleComboBox;

    @FXML
    private ComboBox<String> vehicleTypeComboBox;

    @FXML
    private Button addNewVehicleButton;

    @FXML
    public static ListView<Vehicle>[] parkedVehiclesList;

    @FXML
    private ComboBox<Integer> platformComboBox;

    public static Stage ownerStage;
    private static int oldPlatformNumber = 0;
    public static int numberOfPlatforms = 0;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ObservableList<String> vehicles = FXCollections.observableArrayList("Motorcycle", "Car", "Van");
        vehicleComboBox.setItems(vehicles);
        LinkedList<Vehicle> list = null;
        try {
            Properties property = new Properties();
            property.load(new FileReader("platform.properties"));
            numberOfPlatforms = Integer.parseInt(property.getProperty("NumberOfPlatforms"));
            if(numberOfPlatforms < 1){
                Alert alert = new Alert(Alert.AlertType.ERROR, null, ButtonType.OK);
                alert.setTitle("Error");
                alert.setContentText("Number of platforms can not be lower than 1.");
                alert.showAndWait();
                System.exit(0);
            }
            parkedVehiclesList = new ListView[numberOfPlatforms];
        }catch(Exception ex){
            ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
        }
        for(int i = 0; i < numberOfPlatforms; ++i)
            parkedVehiclesList[i] = new ListView<>();
        try {
            FileInputStream fileInputStream = new FileInputStream("garage.ser");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            list = (LinkedList<Vehicle>)objectInputStream.readObject();
            objectInputStream.close();
            fileInputStream.close();
        }catch(Exception ex){
            ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());

        }
        for(int i = 0; i < numberOfPlatforms; ++i){
            platformComboBox.getItems().add(i + 1);
            parkedVehiclesList[i].setLayoutX(315);
            parkedVehiclesList[i].setLayoutY(75);
            parkedVehiclesList[i].setPrefHeight(275);
            parkedVehiclesList[i].setPrefWidth(275);
            parkedVehiclesList[i].setVisible(false);
            pane.getChildren().add(parkedVehiclesList[i]);
        }
        platformComboBox.setValue(1);
        parkedVehiclesList[0].setVisible(true);
        if(list != null) {
            for (Vehicle veh : list)
                parkedVehiclesList[veh.getPlatformNumber() - 1].getItems().add(veh);
        }
    }

    public void initializeVehicleTypeComboBox(){
        String chosenVehicle = vehicleComboBox.getValue();
        if(chosenVehicle.equals("Motorcycle")) {
            ObservableList<String> vehicleTypes = FXCollections.observableArrayList("Civil", "Police");
            vehicleTypeComboBox.setItems(vehicleTypes);
            vehicleTypeComboBox.setDisable(false);
        }
        else if(chosenVehicle.equals("Car")){
            ObservableList<String> vehicleTypes = FXCollections.observableArrayList("Civil", "Police", "Ambulance");
            vehicleTypeComboBox.setItems(vehicleTypes);
            vehicleTypeComboBox.setDisable(false);
        }
        else{
            ObservableList<String> vehicleTypes = FXCollections.observableArrayList("Civil", "Police", "FireFighter", "Ambulance");
            vehicleTypeComboBox.setItems(vehicleTypes);
            vehicleTypeComboBox.setDisable(false);
        }
    }

    public void enableAddNewVehicleButton() {
        if(vehicleTypeComboBox.getValue() != null)
            addNewVehicleButton.setDisable(false);
    }

    public void showAddNewVehicleScene(){
        try{
            AddNewVehicleController.toAddList = parkedVehiclesList;
            AddNewVehicleController.chosenVehicle = vehicleComboBox.getValue();
            AddNewVehicleController.chosenVehicleType = vehicleTypeComboBox.getValue();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("AddNewVehicle.fxml"));
            Stage newStage = new Stage();
            newStage.initOwner(ownerStage);
            Scene newScene = new Scene((Parent)loader.load());
            newStage.setScene(newScene);
            newStage.initModality(Modality.APPLICATION_MODAL);
            newStage.setTitle("Add New Vehicle");
            AddNewVehicleController.stage = newStage;
            newStage.setResizable(false);
            newStage.showAndWait();
        }catch(Exception ex) {
            ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
        }
    }

    public void removeVehicle(){
        Vehicle toRemoveVehicle = parkedVehiclesList[platformComboBox.getValue() - 1].getSelectionModel().getSelectedItem();
        parkedVehiclesList[platformComboBox.getValue() - 1].getItems().remove(toRemoveVehicle);
    }

    public void exit(){
        ownerStage.close();
        Runtime.getRuntime().exit(0);
    }

    public void previewVehicle(){
        Vehicle selectedVehicle = parkedVehiclesList[platformComboBox.getValue() - 1].getSelectionModel().getSelectedItem();
        PreviewVehicleController.vehicle = selectedVehicle;
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("previewVehicle.fxml"));
            Stage previewStage = new Stage();
            previewStage.initOwner(ownerStage);
            Scene newScene = new Scene((Parent)loader.load());
            previewStage.setScene(newScene);
            previewStage.initModality(Modality.APPLICATION_MODAL);
            previewStage.setTitle("Preview Vehicle");
            previewStage.setResizable(false);
            previewStage.showAndWait();
        }catch(Exception ex) {
            ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
        }
    }

    public void showModifyVehicleScene(){
        try{
            ModifyVehicleController.parkedVehicles = parkedVehiclesList;
            ModifyVehicleController.toModifyVehicle = parkedVehiclesList[platformComboBox.getValue() - 1].getSelectionModel().getSelectedItem();
            FXMLLoader loader = new FXMLLoader(getClass().getResource("ModifyVehicle.fxml"));
            Stage newStage = new Stage();
            newStage.initOwner(ownerStage);
            Scene newScene = new Scene((Parent)loader.load());
            newStage.setScene(newScene);
            newStage.initModality(Modality.APPLICATION_MODAL);
            newStage.setTitle("Modify Vehicle");
            ModifyVehicleController.stage = newStage;
            newStage.setResizable(false);
            newStage.showAndWait();
        }catch(Exception ex) {
            ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
        }
    }

    public void showSimulationSceneAndSave(){
        try {
            FileOutputStream fileOutputStream = new FileOutputStream("garage.ser");
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            LinkedList<Vehicle> list = new LinkedList<>();
            for (int i = 0; i < numberOfPlatforms; ++i) {
                ObservableList<Vehicle> parkedVehicles = parkedVehiclesList[i].getItems();
                for (Vehicle veh : parkedVehicles)
                    list.add(veh);
            }
            objectOutputStream.writeObject(list);
            objectOutputStream.close();
            fileOutputStream.close();

            FXMLLoader loader = new FXMLLoader(getClass().getResource("/UserApp/UserApp.fxml"));
            Stage newStage = new Stage();
            ownerStage.close();
            Scene newScene = new Scene((Parent) loader.load());
            newStage.setScene(newScene);
            newStage.initStyle(StageStyle.UNDECORATED);
            newStage.initModality(Modality.APPLICATION_MODAL);
            newStage.setTitle("SimulationController");
            newStage.setResizable(false);
            newStage.show();
        }catch(Exception ex) {
            ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
        }
    }

    public void platformChange(){
        int newPlatformNumber = platformComboBox.getValue() - 1;
        parkedVehiclesList[oldPlatformNumber].setVisible(false);
        parkedVehiclesList[newPlatformNumber].setVisible(true);
        oldPlatformNumber = newPlatformNumber;

    }
}