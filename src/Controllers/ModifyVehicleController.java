package Controllers;

import Vehicles.Car;
import Vehicles.Van;
import Vehicles.Vehicle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class ModifyVehicleController implements Initializable {

    @FXML
    private TextField nameField;

    @FXML
    private TextField chassisNumberField;

    @FXML
    private TextField engineNumberField;

    @FXML
    private TextField licencePlateField;

    @FXML
    private ComboBox<Integer> platformNumberComboBox;

    @FXML
    private TextField parkingSpotTextField;

    @FXML
    private ComboBox<Integer> numberOfDoorsComboBox;

    @FXML
    private TextField capacityField;

    @FXML
    private ImageView imageViewer;

    @FXML
    private Label numberOfDoorsLabel;

    @FXML
    private Label capacityLabel;

    @FXML
    private Label vehicleLabel;

    @FXML
    private Label vehicleTypeLabel;

    static Vehicle toModifyVehicle;

    static ListView<Vehicle>[] parkedVehicles;

    static Stage stage;

    public static File file;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle){
        nameField.setText(toModifyVehicle.getName());
        chassisNumberField.setText(toModifyVehicle.getChassisNumber());
        engineNumberField.setText(toModifyVehicle.getEngineNumber());
        licencePlateField.setText(toModifyVehicle.getLicencePlate());
        for(int i = 0; i < AdministratorController.numberOfPlatforms; ++i)
            platformNumberComboBox.getItems().add(i + 1);
        platformNumberComboBox.setValue(toModifyVehicle.getPlatformNumber());
        parkingSpotTextField.setText(Integer.toString(toModifyVehicle.getParkingSpot()));
        file = toModifyVehicle.getImageFile();
        if(toModifyVehicle.getImageFile() != null)
            imageViewer.setImage(new Image(toModifyVehicle.getImageFile().toURI().toString()));
        if(toModifyVehicle.getTypeAndPriority() > 0){
            if(toModifyVehicle.getTypeAndPriority() == 1)
                vehicleTypeLabel.setText("Vehicle Type: Police");
            else if(toModifyVehicle.getTypeAndPriority() == 2)
                vehicleTypeLabel.setText("Vehicle Type: Firefighter");
            else
                vehicleTypeLabel.setText("Vehicle Type: Ambulance");
        }
        else
            vehicleTypeLabel.setText("Vehicle Type: Civil");
        if(toModifyVehicle instanceof Car){
            ObservableList<Integer> number = FXCollections.observableArrayList(2, 3, 4, 5);
            numberOfDoorsComboBox.setItems(number);
            numberOfDoorsComboBox.setVisible(true);
            numberOfDoorsLabel.setVisible(true);
            numberOfDoorsComboBox.setValue(((Car) toModifyVehicle).getNumberOfDoors());
            vehicleLabel.setText("Vehicle: Car");
        }
        else if (toModifyVehicle instanceof Van) {
            capacityField.setVisible(true);
            capacityLabel.setVisible(true);
            capacityField.setText(Integer.toString(((Van)(toModifyVehicle)).getCapacity()));
            vehicleLabel.setText("Vehicle: Van");
        }
        else
            vehicleLabel.setText("Vehicle: Motorcycle");
    }

    public void modifyVehicle(){
        parkedVehicles[toModifyVehicle.getPlatformNumber() - 1].getItems().remove(toModifyVehicle);
        if(nameField.getText().trim().isEmpty() || chassisNumberField.getText().trim().isEmpty() || engineNumberField.getText().trim().isEmpty() || licencePlateField.getText().trim().isEmpty() || platformNumberComboBox.getValue() == null || parkingSpotTextField.getText().trim().isEmpty() || file == null){
            Alert alert = new Alert(Alert.AlertType.ERROR, null, ButtonType.OK);
            alert.setHeaderText("Fill all fields and choose picture.");
            alert.showAndWait();
            return;
        }
        if(toModifyVehicle instanceof Car) {
            if(numberOfDoorsComboBox.getValue() == null) {
                Alert alert = new Alert(Alert.AlertType.ERROR, null, ButtonType.OK);
                alert.setHeaderText("Fill all fields and choose picture.");
                alert.showAndWait();
                return;
            }
            ((Car) toModifyVehicle).setNumberOfDoors(numberOfDoorsComboBox.getValue());
        }
        else if(toModifyVehicle instanceof Van){
            if(capacityField.getText().trim().isEmpty()){
                Alert alert = new Alert(Alert.AlertType.ERROR, null, ButtonType.OK);
                alert.setHeaderText("Fill all fields and choose picture.");
                alert.showAndWait();
                return;
            }
            ((Van) toModifyVehicle).setCapacity(Integer.parseInt(capacityField.getText()));
        }
        toModifyVehicle.setName(nameField.getText());
        toModifyVehicle.setChassisNumber(chassisNumberField.getText().toUpperCase());
        toModifyVehicle.setEngineNumber(engineNumberField.getText().toUpperCase());
        toModifyVehicle.setLicencePlate(licencePlateField.getText().toUpperCase());
        toModifyVehicle.setPlatformNumber(platformNumberComboBox.getValue());
        toModifyVehicle.setParkingSpot(Integer.parseInt(parkingSpotTextField.getText()));
            toModifyVehicle.setImage(file);
        parkedVehicles[toModifyVehicle.getPlatformNumber() - 1].getItems().add(toModifyVehicle);
        Alert alert = new Alert(Alert.AlertType.INFORMATION, null, ButtonType.OK);
        alert.setHeaderText("Vehicle modified successfully.");
        alert.showAndWait();
        stage.close();
    }

    public void choosePicture(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("Pictures"));
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        file = fileChooser.showOpenDialog(stage);
        if(file != null)
            imageViewer.setImage(new Image(file.toURI().toString()));
    }
}
