package Controllers;

import Vehicles.Car;
import Vehicles.Van;
import Vehicles.Vehicle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.net.URL;
import java.util.ResourceBundle;

public class PreviewVehicleController implements Initializable {


    @FXML
    private TextField nameTextField;

    @FXML
    private TextField chassisNumberTextField;

    @FXML
    private TextField engineNumberTextField;

    @FXML
    private TextField licencePlateTextField;

    @FXML
    private TextField parkingSpotTextField;

    @FXML
    private ImageView imageView;

    @FXML
    private Label numberOfDoorsLabel;

    @FXML
    private Label capacityLabel;

    @FXML
    private TextField platformNumberTextField;

    @FXML
    private TextField vehicleTextField;

    @FXML
    private TextField vehicleTypeTextField;

    @FXML
    private TextField capacityTextField;

    @FXML
    private TextField numberOfDoorsTextField;

    @FXML
    private Label nameLabel;

    public static Vehicle vehicle;

    public void initialize(URL url, ResourceBundle rb){
        String type;
        int priority = vehicle.getTypeAndPriority();
        if(priority == 0)
            type = "Civil";
        else if(priority == 1)
            type = "Police";
        else if(priority == 2)
            type = "Firefighter";
        else
            type = "Ambulance";
        nameTextField.setText(vehicle.getName());
        nameTextField.setEditable(false);
        chassisNumberTextField.setEditable(false);
        chassisNumberTextField.setText(vehicle.getChassisNumber());
        engineNumberTextField.setEditable(false);
        engineNumberTextField.setText(vehicle.getEngineNumber());
        licencePlateTextField.setEditable(false);
        licencePlateTextField.setText(vehicle.getLicencePlate());
        vehicleTextField.setEditable(false);
        vehicleTypeTextField.setEditable(false);
        vehicleTypeTextField.setText(type);
        platformNumberTextField.setEditable(false);
        platformNumberTextField.setText(Integer.toString(vehicle.getPlatformNumber()));
        parkingSpotTextField.setEditable(false);
        parkingSpotTextField.setText(Integer.toString(vehicle.getParkingSpot()));
        nameLabel.requestFocus();
        if(vehicle.getImageFile() != null)
            imageView.setImage(new Image(vehicle.getImageFile().toURI().toString()));
        if(vehicle instanceof Car){
            vehicleTextField.setText("Car");
            numberOfDoorsLabel.setVisible(true);
            numberOfDoorsTextField.setVisible(true);
            numberOfDoorsTextField.setEditable(false);
            numberOfDoorsTextField.setText(Integer.toString(((Car)vehicle).getNumberOfDoors()));
        }
        else if(vehicle instanceof Van){
            vehicleTextField.setText("Van");
            capacityLabel.setVisible(true);
            capacityTextField.setVisible(true);
            capacityTextField.setEditable(false);
            capacityTextField.setText(Integer.toString(((Van)vehicle).getCapacity()));
        }
        else
            vehicleTextField.setText("Motorcycle");
    }
}