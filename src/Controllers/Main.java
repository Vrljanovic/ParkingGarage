package Controllers;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.fxml.FXMLLoader;
import javafx.stage.StageStyle;
import Logger.ErrorLogger;

import java.util.logging.*;


public class Main extends Application {
    @Override
    public void start(Stage primaryStage) {
       try {
            Pane root = (Pane)FXMLLoader.load(getClass().getResource("Application.fxml"));
            Scene scene = new Scene(root,600,400);
            scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
            primaryStage.setScene(scene);
            primaryStage.setResizable(false);
            primaryStage.initStyle(StageStyle.UNDECORATED);
            primaryStage.show();
            primaryStage.setTitle("Parking Garage Simulator");
            AdministratorController.ownerStage = primaryStage;
       } catch(Exception ex) {
           ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
       }
    }

    public static void main(String[] args) {
        launch(args);
    }
}
