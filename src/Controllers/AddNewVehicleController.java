package Controllers;

import Logger.ErrorLogger;
import UserApp.Payment;
import Vehicles.Car;
import Vehicles.Motorcycle;
import Vehicles.Van;
import Vehicles.Vehicle;
import UserApp.Garage;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;

public class AddNewVehicleController implements Initializable {

    @FXML
    private TextField nameField;

    @FXML
    private TextField chassisNumberField;

    @FXML
    private TextField engineNumberField;

    @FXML
    private TextField licencePlateField;

    @FXML
    private ComboBox<Integer> numberOfDoorsComboBox;

    @FXML
    private TextField capacityField;

    @FXML
    private ImageView imageViewer;

    @FXML
    private ComboBox<Integer> platformNumberComboBox;

    @FXML
    private TextField parkingSpotTextField;

    public static Stage stage;

    public static String chosenVehicle, chosenVehicleType;

    private static File file;

    public static ListView<Vehicle>[] toAddList;

    @Override
    public void initialize(URL url, ResourceBundle rb){
        for(int i = 0; i < AdministratorController.numberOfPlatforms; ++i)
            platformNumberComboBox.getItems().add(i + 1);
        if(chosenVehicle.equals("Car")) {
            ObservableList<Integer> number = FXCollections.observableArrayList(2, 3, 4, 5);
            numberOfDoorsComboBox.setItems(number);
            numberOfDoorsComboBox.setVisible(true);
        }
        if(chosenVehicle.equals("Van"))
            capacityField.setVisible(true);
    }

    public void choosePicture(){
        FileChooser fileChooser = new FileChooser();
        fileChooser.setInitialDirectory(new File("Pictures"));
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.bmp"));
        file = fileChooser.showOpenDialog(stage);
        if(file != null)
            imageViewer.setImage(new Image(file.toURI().toString()));
    }

    public void addNewVehicle(){

        if(Garage.isGarageFull()) {
            showErrorAlert("Garage is full!");
            stage.close();
            return;
        }
        int priority;
        if(chosenVehicleType.equals("Civil"))
            priority = 0;
        else if(chosenVehicleType.equals("Police"))
            priority = 1;
        else if(chosenVehicleType.equals("Firefighter"))
            priority = 2;
        else
            priority = 3;
        if(nameField.getText().trim().isEmpty() || chassisNumberField.getText().trim().isEmpty() || engineNumberField.getText().trim().isEmpty() || licencePlateField.getText().trim().isEmpty() || platformNumberComboBox.getValue() == null || parkingSpotTextField.getText().trim().isEmpty() || file == null){
            showErrorAlert("Fill all fields and choose picture.");
            return;
        }
        int parkingSpot = 0;
        int platform = platformNumberComboBox.getValue();
        try {
           parkingSpot  = Integer.parseInt(parkingSpotTextField.getText());
        }catch(Exception ex){
            showErrorAlert("Parking Spot Incorrect!");
            ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
        }
        if(parkingSpot < 1 || parkingSpot > 28){
            showErrorAlert("Parking spot number can be between 1 and 28");
            return;
        }
        if(!Garage.isParkingSpotFree(platformNumberComboBox.getValue(), parkingSpot)){
            int[] parkingPlace = Garage.parkVehicleRandom();
            platform = parkingPlace[0];
            parkingSpot = parkingPlace[1];
            showErrorAlert("Parking Spot is not free. Vehicle will be parked on " + parkingPlace[0] + " platform, " + parkingPlace[1] + " parking spot.");
        }if(chosenVehicle.equals("Motorcycle")){
            Motorcycle motorcycle = new Motorcycle(nameField.getText(), chassisNumberField.getText().toUpperCase(), engineNumberField.getText().toUpperCase(), licencePlateField.getText().toUpperCase(), priority, false, platform, parkingSpot, file);
            toAddList[platformNumberComboBox.getValue() - 1].getItems().add(motorcycle);
            Payment.add(motorcycle);
        }
        else if(chosenVehicle.equals("Car")){
            if(numberOfDoorsComboBox.getValue() == null) {
                showErrorAlert("Fill all fields and choose picture.");
                return;
            }
            Car car = new Car(nameField.getText(), chassisNumberField.getText().toUpperCase(), engineNumberField.getText().toUpperCase(), licencePlateField.getText().toUpperCase(), priority, false, platform, parkingSpot, numberOfDoorsComboBox.getValue(), file);
            toAddList[platformNumberComboBox.getValue() - 1].getItems().add(car);
            Payment.add(car);
        }
        else{
            if(capacityField.getText().trim().isEmpty()){
                showErrorAlert("Fill all fields and choose picture.");
                return;
            }
            try {
                Integer vanCapacity = Integer.parseInt(capacityField.getText());
                if(vanCapacity<0 || vanCapacity >3500)
                {
                    showErrorAlert("Capacity can be from 0 - 3500 kg");
                    return;
                }
            }catch(Exception ex){
                showErrorAlert("Capacity Incorrect!");
                ErrorLogger.logger.log(Level.SEVERE, ex.getMessage());
                return;
            }
            Van van = new Van(nameField.getText(), chassisNumberField.getText().toUpperCase(), engineNumberField.getText().toUpperCase(), licencePlateField.getText().toUpperCase(), priority, false, platform, parkingSpot, Integer.parseInt(capacityField.getText()), file);
            toAddList[platformNumberComboBox.getValue() - 1].getItems().add(van);
            Payment.add(van);
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION, null, ButtonType.OK);
        alert.setHeaderText("Vehicle added successfully.");
        alert.showAndWait();
        stage.close();
    }

    private void showErrorAlert(String message){
        Alert alert = new Alert(Alert.AlertType.ERROR, null, ButtonType.OK);
        alert.setHeaderText(message);
        alert.showAndWait();
    }

}